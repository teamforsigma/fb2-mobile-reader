angular.module('starter.controllers', [])

.controller('MainController', function($scope, $ionicSideMenuDelegate) {
        $scope.toggleLeft = function() {
            $ionicSideMenuDelegate.toggleLeft();
        };
})

.controller('HomeController', function($scope, $state, fb2Reader, currentBook) {
        $scope.showBook = false;
        $scope.openFileDialog=function() {
            fileChooser.open(function(uri) {
                fb2Reader.read(uri,
                    function (book) {
                        currentBook.setBook(book);
                        $state.transitionTo("menu.book");
                    }, function() {

                });
            });
        }
})

.controller('BookController', function($scope, currentBook) {
    $scope.book = currentBook.getBook();
});

