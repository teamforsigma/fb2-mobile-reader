angular.module('starter.services', [])
.factory('fs', function() {
    var fileSystem;
    this.get = function(callback) {
        if (fileSystem == null) {
            var func = window.webkitRequestFileSystem || window.requestFileSystem;
            func(window.PERSISTENT, 1024 * 1024, function(fs) {
                fileSystem = fs;
                console.log("FS request success");
                callback(fs);
            }, function(e){
                console.log("FS request failed");
            });
        } else {
            callback(fileSystem);
        }
    };
    return this;
})

.service('fileService', function(fs) {
    this.readAsText = function(fullPath, charset, onSuccess) {
        fs.get(function(fileSystem) {
            var dir = fileSystem.root.nativeURL;
            if (fullPath.indexOf(dir) == 0) {
                fullPath = fullPath.substring(dir.length, fullPath.length);
            }
            fileSystem.root.getFile(fullPath, null, function(file) {
                file.file(function(file) {
                    var reader = new FileReader();
                    reader.onloadend = function(evt) {
                        onSuccess(evt.target.result);
                    };
                    reader.readAsText(file, charset);
                });
            }, function(e) {
                console.log(e);
            });
        });
    }
})

.service('fb2HtmlPrepare', function() {
    this.format = function(doc) {
        var elementCount = 0;
        var body = jQuery(doc).find("body");
        function getBinary(binaryId) {
            var image = "";
            jQuery(doc).find("binary").each(function () {
                if (jQuery(this).attr('id') == binaryId) {
                    image = jQuery(this).text();
                    //break;
                }
            });
        }
        function sectionHtml(src,_level, isNotes){
            var html = "";
            var level = _level+2;
            if (level>6){
                level = 6;
            }
            elementCount += 1;
            //console.log(src)
            jQuery(src).children().each(function () {
                elementCount += 1;
                switch ((this).tagName) {
                    case 'image':

                        var href = "";
//                        jQuery.each(this.attributes, function(index, value){
//                            if (value.indexOf("href") > -1){
//                                href = value;
//                            }
//                        });
//
//                        var id = jQuery(this).attr(href).substr(1);
//                        html += "<div id='section"+elementCount+"' class='bookimage'><img class='section-illustration' id="+ id +" src='data:image/gif;base64," + getBinary(id) + "' /></div>";
                        break;
                    case 'title':
                        if (isNotes && _level>0){
                            jQuery(this).children("p").each(function(){
                                html += "<a id='"+jQuery(src).attr("id")+"'><h"+level+" class='section-title'>" + jQuery(this).text() + "</h"+level + "></a>";
                            });

                        }else{
                            jQuery(this).children("p").each(function(){
                                html += "<h"+level+" id='section"+elementCount+"' class='section-title'>" + jQuery(this).text() + "</h"+level + ">";
                            });
                        }
                        break;
                    case 'epigraph': {

                        jQuery(this).children().each(function(){
                            switch ((this).tagName) {
                                case 'p':
                                    html += "<p id='section"+elementCount+"' class='section-epigraph'>" + jQuery(this).text() + "</p>";
                                    break;
                                case 'text-author':
                                    html += "<p id='section"+elementCount+"' class='section-epigraph-author'>" + jQuery(this).text() + "</p><br/>";
                                    break;
                                case 'empty-line':
                                    html += "<br/>";
                                    break;
                            }
                        });
                        break;
                    }
                    case 'section':
                        html += sectionHtml(jQuery(this), _level+1,isNotes);
                        break;
                    case 'p':
                        if (jQuery(this).children("a").size()>0){

                            jQuery(this).children("a").each(function(){
                                elementCount += 1;
                                var href = "";
//                                jQuery.each(jQuery(this).listAttributes(), function(index, value){
//                                    if (value.indexOf("href") > -1){
//                                        href = value;
//                                    }
//                                });
//
//                                jQuery(this).text(" [<a id='section"+elementCount+"' href='"+jQuery(this).attr(href)+"'>" + jQuery(this).text() + "</a>]");
                            });

                            html += "<p id='section"+elementCount+"' class='body-paragpraph'>" + jQuery(this).text() + "</p>";
                        }else{
                            html += "<p id='section"+elementCount+"' >" + jQuery(this).text() + "</p>";
                        }
                        break;
                    case 'poem':{
                        html += "<p id='section"+elementCount+"' class='poem'>";
                        jQuery(this).children().each(function(){
                            elementCount += 1;
                            switch ((this).tagName) {
                                case 'title':
                                    html += "<h1 id='section"+elementCount+"' class='poem-title'>" + jQuery(this).text() + "</h1>";
                                    break;
                                case 'epigraph':
                                    html += "<span id='section"+elementCount+"' class='poem-epigraph'>" + jQuery(this).text() + "</span><br/>";
                                    break;
                                case 'stanza':
                                    html += "<span id='section"+elementCount+"' class='poem-stanza'>";
                                    jQuery(this).children("v").each(function(){
                                        html += jQuery(this).text() + "<br/>";
                                    });
                                    html += "</span><br/>";
                                    break;
                                case 'empty-line':
                                    html += "<br/>";
                                    break;
                            }
                        });

                        html += "</p>";
                    }
                    break;
                }
            });
            return html;
        }

        return sectionHtml(body, 0, false);
    };
})

.service('fb2Parser', function(fb2HtmlPrepare) {
    this.parseFb2 = function(text, successCallback) {
        var book = {};
        var xml = jQuery.parseXML(text);
        var titleInfo = jQuery(xml).find("title-info");
        book.title = {};
        book.title.author = {};
        book.title.author.firstname = jQuery(titleInfo).find("first-name").text();

        book.text = fb2HtmlPrepare.format(xml);
        successCallback(book);
    }
})

.service('fb2Reader', function(fileService, fb2Parser) {
    this.read = function(filePath, successCallback) {
        fileService.readAsText(filePath, 'UTF8', function(text) {
            var str = text.substr(0, text.indexOf("\n"));
            var matches = str.match('encoding="(.*?)"');
            if (matches != 0) {
                var encoding = matches[1];
                text = null;
                fileService.readAsText(filePath, encoding, function(text) {
                    fb2Parser.parseFb2(text, successCallback);
                });
            } else {
                fb2Parser.parseFb2(text, successCallback);
            }
        });
    }
})

.service('currentBook', function() {
    this.book = {};
    this.setBook = function(b) {
        book = b;
    };
    this.getBook = function() {
        return book;
    }
});
